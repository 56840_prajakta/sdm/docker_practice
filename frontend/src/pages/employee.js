


const EmployeePage = () => {
 
  return (
    <div>
      <h1>Employee List</h1>
      <table className="table table-stripped">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Salary</th>
            <th>Age</th>
          </tr>
        </thead>

        <tbody>
         
              <tr>
                <td>1</td>
                <td>Prajakta</td>
                <td>548765.025</td>
                <td>24</td>
              </tr>

              <tr>
                <td>2</td>
                <td>Pushpanjali</td>
                <td>601257.014</td>
                <td>23</td>
              </tr>         

              <tr>
                <td>3</td>
                <td>Sanket</td>
                <td>45863.25</td>
                <td>24</td>
              </tr>

              <tr>
                <td>4</td>
                <td>p</td>
                <td>45863.25</td>
                <td>24</td>
              </tr>
        </tbody>
      </table>
    </div>
  )
}

export default EmployeePage
